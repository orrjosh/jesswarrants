(defclass warrant us.joshorr.jesswarrants.WarrantBean)
(defclass service us.joshorr.jesswarrants.ServiceBean)
(defclass operator-alert us.joshorr.jesswarrants.OperatorAlertBean)

(deftemplate countyBorderRelationship (slot county) (slot neighbor))
(deffacts counties
    (countyBorderRelationship (county "Kane") (neighbor "DeKalb"))
    (countyBorderRelationship (county "DeKalb") (neighbor "Kane"))
    (countyBorderRelationship (county "DeKalb") (neighbor "Kendall"))
    (countyBorderRelationship (county "Kane") (neighbor "Kendall"))
    (countyBorderRelationship (county "Kane") (neighbor "DuPage"))
    (countyBorderRelationship (county "DeKalb") (neighbor "Ogle"))
    (countyBorderRelationship (county "Alexander") (neighbor "Union"))
    (countyBorderRelationship (county "Alexander") (neighbor "Pulaski"))
)

(defrule high-class-pre-trial-cf-nationwide "Send warrants for very serious crimes to nationwide"
    (warrant (caseType "CF") (casePhase "PR") 
        (offenseClass ?oc&:(or (eq ?oc "M")(or (eq ?oc "X")(or (eq ?oc "1")(eq ?oc "2")))))
        (fileName ?fn))
    =>
    (definstance service(new us.joshorr.jesswarrants.ServiceBean "nationwide" null ?fn ))
    )
(defrule mid-class-pre-trial-cf-statewide "Send warrants for non-serious felonies to statewide"
    (warrant(caseType "CF") (casePhase "PR")
        (offenseClass ?oc&:(or (eq ?oc "3")(eq ?oc "4")))
        (fileName ?fn))
    =>
    (definstance service(new us.joshorr.jesswarrants.ServiceBean "statewide" null ?fn))
    ) 
(defrule special-class-pre-trial-cf-surrounding "Send special CF class warrants to surrounding counties"
    (warrant(caseType "CF") (casePhase "PR")
    	(offenseClass ?oc&:(not (or (eq ?oc "3")(or (eq ?oc "X")(or (eq ?oc "1")(or(eq ?oc "2")(or (eq ?oc "M") (eq ?oc "4"))))))))
        (fileName ?fn) (county ?c)
    )
    =>
    (definstance service(new us.joshorr.jesswarrants.ServiceBean "surrounding-counties" ?c ?fn ))
    )

(defrule non-pre-trial-cf-statewide
    (warrant (caseType "CF") 
       (casePhase ?cp&:(not (eq ?cp "PR")))
        (fileName ?fn))
    =>
    (definstance service(new us.joshorr.jesswarrants.ServiceBean "statewide" null ?fn))
    )
(defrule non-special-non-pre-trial-send-operator-alert "Standard felonies go statewide with surrounding state/nationwide option"
    (warrant (caseType "CF") 
       (casePhase ?cp&:(not (eq ?cp "PR")))
        (offenseClass ?oc&:(or (eq ?oc "3")(or (eq ?oc "X")(or (eq ?oc "1")(or(eq ?oc "2")(or (eq ?oc "M") (eq ?oc "4")))))))
        (fileName ?fn) (county ?c) (caseNumber ?cn))
    =>
    (definstance operator-alert (new us.joshorr.jesswarrants.OperatorAlertBean ?cn ?fn "possible-surrounding-states"))
	(definstance operator-alert (new us.joshorr.jesswarrants.OperatorAlertBean ?cn ?fn "possible-nationwide"))
    )

(defrule all-cm-cases-surrounding "All cm cases go to surrounding counties regardless of case phase or classification"
    (warrant (caseType "CM")
        (fileName ?fn) (county ?c))
    =>
    (definstance service(new us.joshorr.jesswarrants.ServiceBean "surrounding-counties" ?c ?fn))
    )
(defrule all-dt-cases-statewide "All dt cases go statewide regardless of case phase or classification"
    (warrant (caseType "DT")
        (fileName ?fn) (county ?c))
    =>
    (definstance service(new us.joshorr.jesswarrants.ServiceBean "statewide" null ?fn))
    )
(defrule misdemeanor-traffic-surrounding "TR cases that are also misdemeanors go to surrounding counties"
    (warrant(caseType "TR")
        (offenseClass ?oc&:(or (eq ?oc "A")(eq ?oc "B")))
        (fileName ?fn) (county ?c))
    =>
    (definstance service(new us.joshorr.jesswarrants.ServiceBean "surrounding-counties" ?c ?fn))
    )
(defrule minor-offenses-same-county "Minor TR,CV,CC,OV warrants apply only to issuing county"
    (warrant (caseType ?ct&:(or (eq ?ct "TR")(or (eq ?ct "CV") (or (eq ?ct "CC")(eq ?ct "OV")))))
        (offenseClass ?oc&:(not(and (eq ?ct "TR")(or (eq ?oc "A")(eq ?oc "B")))))
        (fileName ?fn) (county ?c))
    =>
    (definstance service(new us.joshorr.jesswarrants.ServiceBean "same-county" ?c ?fn))
    )
(defrule pretrial-felony-juvenile-cases-nationwide "All Juvenile felony pre-trial cases are nationwide warrants"
    (warrant (caseType "JD") (casePhase "PR")
    	(offenseClass ?oc&:(or (eq ?oc "3")(or (eq ?oc "X")(or (eq ?oc "1")(or(eq ?oc "2")(or (eq ?oc "M") (eq ?oc "4")))))))
        (fileName ?fn))
    =>
    (definstance service(new us.joshorr.jesswarrants.ServiceBean "nationwide" null ?fn))
    )
    
(defrule non-pretrial-felony-juvenile-cases-statewide "All Juvenile felony non-pre-trial cases are statewide warrants"
    (warrant (caseType "JD") (casePhase ?cp&:(not (eq ?cp "PR")))
        (offenseClass ?oc&:(or (eq ?oc "3")(or (eq ?oc "X")(or (eq ?oc "1")(or(eq ?oc "2")(or (eq ?oc "M") (eq ?oc "4")))))))
        (fileName ?fn)
        )
    =>
    (definstance service(new us.joshorr.jesswarrants.ServiceBean "statewide" null ?fn))
    )
(defrule non-pretrial-misdemeanor-juvenile-cases-surrounding "All Juvenile misdemeanor non-pre-trial cases are surrounding county warrants"
    (warrant (caseType "JD") (casePhase ?cp&:(not (eq ?cp "PR")))
        (offenseClass ?oc&:(not(or (eq ?oc "3")(or (eq ?oc "X")(or (eq ?oc "1")(or(eq ?oc "2")(or (eq ?oc "M") (eq ?oc "4"))))))))
        (fileName ?fn) (county ?c)
        )
    =>
    (definstance service(new us.joshorr.jesswarrants.ServiceBean "surrounding-counties" ?c ?fn))
    )
    
(defrule alert-operator-no-service "Send the operator a message if the 
    warrant doesn't match any service rules"
    (declare (salience -50))
    (not (service))
    (warrant (caseNumber ?cn) (fileName ?fn))
    =>
    (definstance operator-alert 
        (new us.joshorr.jesswarrants.OperatorAlertBean ?cn ?fn "no-service-found"))
    )

(defrule select-surrounding-counties "Create service facts for sending to surrounding counties "
    (declare (salience -10))
    (service (serviceType "surrounding-counties") (recipient ?c) (fileName ?fn))
    (countyBorderRelationship (county ?c) (neighbor ?n))
    =>
    (definstance service(new us.joshorr.jesswarrants.ServiceBean "surrounding-recipient" ?n  ?fn))
    )
