package us.joshorr.jesswarrants;

public class ServiceBean {
	String serviceType;
	String recipient;
	String fileName;
	
	public ServiceBean(){
		
	}
	public ServiceBean(String serviceType, String recipient, String fileName){
		this.serviceType = serviceType;
		this.recipient = recipient;
		this.fileName = fileName;
	}
	
	public String getServiceType() {
		return serviceType;
	}
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	public String getRecipient() {
		return recipient;
	}
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	
}
