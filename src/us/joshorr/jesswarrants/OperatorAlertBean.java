package us.joshorr.jesswarrants;

public class OperatorAlertBean {
	String caseNumber;
	String fileName;
	String AlertCause;
	public String getCaseNumber() {
		return caseNumber;
	}
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getAlertCause() {
		return AlertCause;
	}
	public void setAlertCause(String alertCause) {
		AlertCause = alertCause;
	}
	@Override
	public String toString() {
		return "OperatorAlert [caseNumber=" + caseNumber + ", fileName="
				+ fileName + ", AlertCause=" + AlertCause + "]";
	}
	public OperatorAlertBean(String caseNumber, String fileName, String alertCause) {
		super();
		this.caseNumber = caseNumber;
		this.fileName = fileName;
		AlertCause = alertCause;
	}
	
	

}
