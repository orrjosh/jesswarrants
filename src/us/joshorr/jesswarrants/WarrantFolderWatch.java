package us.joshorr.jesswarrants;


import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Random;

import org.apache.pdfbox.pdmodel.PDDocument;

import static java.nio.file.StandardWatchEventKinds.*;

public class WarrantFolderWatch {
	WatchService wc;
	String listenPath="c:/warrant/";
	String movePath="c:/warrant/done/";

	public WarrantFolderWatch() throws Exception {
		Path dir = FileSystems.getDefault().getPath(listenPath);
		WatchService wc = FileSystems.getDefault().newWatchService();
		// WatchKey watchKey = dir.register(wc, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
		WatchKey watchKey = dir.register(wc, ENTRY_CREATE);

		while (true) {
			watchKey = wc.take();
			for (WatchEvent<?> event : watchKey.pollEvents()) {
				WatchEvent<Path> watchEvent = cast(event);
				
				String fileName = listenPath+watchEvent.context().getFileName();
				Path source = FileSystems.getDefault().getPath(fileName);
				if(fileName.endsWith("pdf")){
					Path target = FileSystems.getDefault().getPath("c:/warrant/done/"+source.getFileName());
					boolean moved=false;
					int faultCount=10;
					while(!moved){
						try{
							Files.move(source, target);
							moved=true;
						}catch(NoSuchFileException nsfe){
							if(faultCount--==0){
								nsfe.printStackTrace();
								throw new RuntimeException("File cannot be found, check folders for problem");
							}
						}catch(FileAlreadyExistsException e){
							if(faultCount--==0){
								throw new RuntimeException("File cannot be moved, check folders for problem");
							}
							String[] fnTokens = source.getFileName().toString().split("\\.");
							
							int randomSuffix = new Random().nextInt(1000000);
							
							target = FileSystems.getDefault().getPath("c:/warrant/done/"+fnTokens[0]+randomSuffix+"."+fnTokens[1]);
							
						}
					}
					fileName = target.toString();
					PDDocument doc = PDDocument.load(new File(fileName));
					IncomingWarrantParser parser = new IncomingWarrantParser(doc, fileName);
					parser.run();
//					new Thread(parser).start();
				}
				watchKey.reset();
			}
		}
	}

	@SuppressWarnings("unchecked")
	static <T> WatchEvent<T> cast(WatchEvent<?> event) {
		return (WatchEvent<T>) event;
	}

	public static void main(String args[]) throws Exception {
		new WarrantFolderWatch();
	}
}
