package us.joshorr.jesswarrants.test;

import static org.junit.Assert.*;

import java.util.Iterator;

import jess.Filter;
import jess.Rete;
import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import us.joshorr.jesswarrants.OperatorAlertBean;
import us.joshorr.jesswarrants.ServiceBean;
import us.joshorr.jesswarrants.WarrantBean;




public class BasicTest {
	WarrantBean classX = new WarrantBean("CF","PR", "X","DeKalb", "x.pdf", "13CFX");
	WarrantBean classM = new WarrantBean("CF","PR", "M","DeKalb", "m.pdf", "13CFM");
	WarrantBean class1 = new WarrantBean("CF","PR", "1","DeKalb", "1.pdf", "13CF1");
	WarrantBean class2 = new WarrantBean("CF","PR", "2","DeKalb", "2.pdf", "13CF2");
	WarrantBean class3 = new WarrantBean("CF","PR", "3","DeKalb", "3.pdf", "13CF3");
	WarrantBean class4 = new WarrantBean("CF","PR", "4","DeKalb", "4.pdf", "13CF4");
	WarrantBean classS = new WarrantBean("CF","PR", "S","DeKalb", "S.pdf", "13CFS");
	WarrantBean classA = new WarrantBean("CM","PR", "A","DeKalb", "A.pdf", "13CMA");
	WarrantBean classB = new WarrantBean("CM","PS", "B","DeKalb", "B.pdf", "13CMB");
	WarrantBean classC = new WarrantBean("CM","FP", "C","DeKalb", "C.pdf", "13CMC");
	WarrantBean dtPS   = new WarrantBean("DT","PS", "C","DeKalb", "DTPS.pdf", "13DTPS");
	WarrantBean dtPR   = new WarrantBean("DT","PR", "C","DeKalb", "DTPR.pdf", "13DTPR");
	WarrantBean trA    = new WarrantBean("TR","PR", "A","DeKalb", "TRA.pdf", "13TRA");
	WarrantBean trB    = new WarrantBean("TR","PR", "B","DeKalb", "TRB.pdf", "13TRB");
	WarrantBean trP    = new WarrantBean("TR","PR", "P","DeKalb", "TRP.pdf", "13TRP");
	WarrantBean ov     = new WarrantBean("OV","PR", "A","DeKalb", "OV.pdf", "13OV1");
	WarrantBean cv     = new WarrantBean("CV","PR", "B","DeKalb", "CV.pdf", "13CV1");
	WarrantBean cc     = new WarrantBean("CC","PR", "C","DeKalb", "CC.pdf", "13CC1");
	WarrantBean jdPreX = new WarrantBean("JD","PR", "X","DeKalb", "JDPRX.pdf", "13JDXPRE");
	WarrantBean jdPostX= new WarrantBean("JD","PS", "X","DeKalb", "JDPSX.pdf", "13JDXPOST");
	WarrantBean jdM    = new WarrantBean("JD","PR", "M","DeKalb", "JDM.pdf", "13JDM");
	WarrantBean jd1    = new WarrantBean("JD","PR", "1","DeKalb", "JD1.pdf", "13JD1");
	WarrantBean jd2    = new WarrantBean("JD","PR", "2","DeKalb", "JD2.pdf", "13JD2");
	WarrantBean jd3    = new WarrantBean("JD","PR", "3","DeKalb", "JD3.pdf", "13JD3");
	WarrantBean jd4    = new WarrantBean("JD","PR", "4","DeKalb", "JD4.pdf", "13JD4");
	WarrantBean jdAPost= new WarrantBean("JD","PS", "A","DeKalb", "JDA.pdf", "13JDA");
	WarrantBean jdAPre = new WarrantBean("JD","PR", "A","Dekalb", "JDAPRE.pdf","13JDA");
	@Before
	public void setUp(){
	
	}

	@Test
	public void testBuildReteAndRun() throws Exception{
		Rete r = new Rete();
		r.batch("DistributionEngine.clp");
		WarrantBean wb = new WarrantBean();
		wb.setCasePhase("PR");
		wb.setCaseType("CF");
		wb.setCounty("DeKalb");
		wb.setFileName("testFile1.pdf");
		wb.setOffenseClass("X");
		r.definstance("warrant", wb, false);
		r.run();
	}
	
	@Test
	public void testPretrialMajorCFNationwide()throws Exception{
		Rete r = new Rete();
		r.reset();
		r.batch("DistributionEngine.clp");
		
		
		r.definstance("warrant", classM, false);
		r.definstance("warrant", classX, false);
		r.definstance("warrant", class1, false);
		r.definstance("warrant", class2, false);
//		r.definstance("warrant", class3, false);
//		r.definstance("warrant", class4, false);
		
		r.reset();
		r.run();		
		Iterator<ServiceBean> i = r.getObjects(new Filter.ByClass(ServiceBean.class));
		int count=0;
//		System.out.println("*******Majors-PR*******");
		while(i.hasNext()){
			count++;
			ServiceBean sb = i.next();
			assertTrue(sb.getServiceType().equals("nationwide"));
//			System.out.println("FileName: "+ sb.getFileName() + " to: "+sb.getRecipient());
		}
		assertTrue(count==4);
		
	}
	@Test
	public void testPretrialLesserCFStatewide()throws Exception{
		Rete r = new Rete();
		r.reset();
		r.batch("DistributionEngine.clp");
		
		
//		r.definstance("warrant", classM, false);
//		r.definstance("warrant", classX, false);
//		r.definstance("warrant", class1, false);
//		r.definstance("warrant", class2, false);
		r.definstance("warrant", class3, false);
		r.definstance("warrant", class4, false);
		
		r.reset();
		r.run();		
		Iterator<ServiceBean> i = r.getObjects(new Filter.ByClass(ServiceBean.class));
		int count=0;
//		System.out.println("*******Minors-PR*******");
		while(i.hasNext()){
			count++;
			ServiceBean sb = i.next();
			assertTrue(sb.getServiceType().equalsIgnoreCase("statewide"));
//			System.out.println("FileName: "+ sb.getFileName() + " to: "+sb.getRecipient());
		}
		assertTrue(count==2);
		
	}
	@Test
	public void testPretrialSpecialCFSurrounding()throws Exception{
		Rete r = new Rete();
		r.reset();
		r.batch("DistributionEngine.clp");
		
		
//		r.definstance("warrant", classM, false);
//		r.definstance("warrant", classX, false);
//		r.definstance("warrant", class1, false);
		r.definstance("warrant", classS, false);
//		r.definstance("warrant", class3, false);
//		r.definstance("warrant", class4, false);
		
		r.reset();
		r.run();		
		Iterator<ServiceBean> i = r.getObjects(new Filter.ByClass(ServiceBean.class));
		int count=0;
//		System.out.println("*******Specials-PR*******");
//		r.executeCommand("(facts)");
		while(i.hasNext()){
			count++;
			ServiceBean sb = i.next();
			
			assertTrue(sb.getServiceType().equalsIgnoreCase("surrounding-counties")
					||sb.getServiceType().equalsIgnoreCase("surrounding-recipient"));
			if(sb.getServiceType().equalsIgnoreCase("surrounding-recipient")){
//				System.out.println("Surrounding Recipient: " + sb.getRecipient() + " file name: " + sb.getFileName());
			}
//			);
//			System.out.println("FileName: "+ sb.getFileName() + " to: "+sb.getRecipient());
		}
		assertTrue(count>=1);
		
	}
	@Test
	public void testNotPretrialStandardCFStatewide()throws Exception{
		Rete r = new Rete();
		r.reset();
		r.batch("DistributionEngine.clp");
		classM.setCasePhase("PS");
		classX.setCasePhase("PS");
		class1.setCasePhase("PS");
		class2.setCasePhase("PS");
		class3.setCasePhase("PS");
		class4.setCasePhase("PS");
		classS.setCasePhase("PS");
		
		r.definstance("warrant", classM, false);
		r.definstance("warrant", classX, false);
		r.definstance("warrant", class1, false);
		r.definstance("warrant", class2, false);
		r.definstance("warrant", classS, false);
		r.definstance("warrant", class3, false);
		r.definstance("warrant", class4, false);
	
		
		r.reset();
		r.run();		
		Iterator<OperatorAlertBean> i = r.getObjects(new Filter.ByClass(OperatorAlertBean.class));
		int count=0;
//		System.out.println("*******Standards-PS*******");
		while(i.hasNext()){
			count++;
			OperatorAlertBean oab = i.next();
			assertTrue(oab.getAlertCause().equalsIgnoreCase("possible-surrounding-states")
					||oab.getAlertCause().equalsIgnoreCase("possible-nationwide"));
//			System.out.println("Operator Alert, Case Number:"+oab.getCaseNumber()+" Cause:"+oab.getAlertCause());
		}
		assertTrue(count==12);
		Iterator<ServiceBean> i2 = r.getObjects(new Filter.ByClass(ServiceBean.class));
		count=0;
		while(i2.hasNext()){
			ServiceBean sb = i2.next();
			count++;
		}
//		System.out.println("Count:"+count);
		assertTrue(count == 7);
		classM.setCasePhase("PR");
		classX.setCasePhase("PR");
		class1.setCasePhase("PR");
		class2.setCasePhase("PR");
		class3.setCasePhase("PR");
		class4.setCasePhase("PR");
		classS.setCasePhase("PR");
		
	}
	@Test
	public void testCMSurrounding()throws Exception{
		Rete r = new Rete();
		r.reset();
		r.batch("DistributionEngine.clp");
		
		
		
		r.definstance("warrant", classA, false);
		r.definstance("warrant", classB, false);
		r.definstance("warrant", classC, false);
		
		
		r.reset();
		r.run();		
		Iterator<ServiceBean> i = r.getObjects(new Filter.ByClass(ServiceBean.class));
		int count=0;
//		System.out.println("*******All-CM*******");
		while(i.hasNext()){
			count++;
			ServiceBean sb = i.next();
			assertTrue(sb.getServiceType().equalsIgnoreCase("surrounding-counties")
					||sb.getServiceType().equalsIgnoreCase("surrounding-recipient"));
			if(sb.getServiceType().equalsIgnoreCase("surrounding-recipient")){
//				System.out.println("Surrounding Recipient: " + sb.getRecipient() + " file name: " + sb.getFileName());
			}
//			System.out.println("FileName: "+ sb.getFileName() + " to: "+sb.getRecipient());
		}
		assertTrue(count>=3);
	}
	@Test
	public void testDTStatewide()throws Exception{
		Rete r = new Rete();
		r.reset();
		r.batch("DistributionEngine.clp");
		
		
		
		r.definstance("warrant", dtPS, false);
		r.definstance("warrant", dtPR, false);
		
		
		
		r.reset();
		r.run();		
		Iterator<ServiceBean> i = r.getObjects(new Filter.ByClass(ServiceBean.class));
		int count=0;
//		System.out.println("*******All-DT*******");
		while(i.hasNext()){
			count++;
			ServiceBean sb = i.next();
			assertTrue(sb.getServiceType().equalsIgnoreCase("statewide"));
//			System.out.println("FileName: "+ sb.getFileName() + " to: "+sb.getRecipient());
		}
		assertTrue(count==2);
	}
	@Test
	public void testCMTRSurrounding()throws Exception{
		Rete r = new Rete();
		r.reset();
		r.batch("DistributionEngine.clp");
		
		

		r.definstance("warrant", trA, false);
		r.definstance("warrant", trB, false);
		

		
		r.reset();
		r.run();		
//		r.executeCommand("(facts)");
		Iterator<ServiceBean> i = r.getObjects(new Filter.ByClass(ServiceBean.class));
		int count=0;
//		System.out.println("*******CM-TR*******");
		while(i.hasNext()){
			count++;
			ServiceBean sb = i.next();
//			System.out.println(sb.getServiceType());
			assertTrue(sb.getServiceType().equalsIgnoreCase("surrounding-counties")
					||sb.getServiceType().equalsIgnoreCase("surrounding-recipient"));
			if(sb.getServiceType().equalsIgnoreCase("surrounding-recipient")){
//				System.out.println("Surrounding Recipient: " + sb.getRecipient() + " file name: " + sb.getFileName());
			}
//			System.out.println("FileName: "+ sb.getFileName() + " to: "+sb.getRecipient());
		}
		assertTrue(count>=2);
	}
	@Test
	public void testMinorOffenses()throws Exception{
		Rete r = new Rete();
		r.reset();
		r.batch("DistributionEngine.clp");
		
		

		r.definstance("warrant", trP, false);
		r.definstance("warrant", cv, false);
		r.definstance("warrant", cc, false);
		r.definstance("warrant", ov, false);
		

		
		r.reset();
		r.run();		
//		r.executeCommand("(facts)");
		Iterator<ServiceBean> i = r.getObjects(new Filter.ByClass(ServiceBean.class));
		int count=0;
//		System.out.println("*******CM-TR*******");
		while(i.hasNext()){
			count++;
			ServiceBean sb = i.next();
//			System.out.println(sb.getServiceType());
			assertTrue(sb.getServiceType().equalsIgnoreCase("same-county"));
//			System.out.println("FileName: "+ sb.getFileName() + " to: "+sb.getRecipient());
		}
		assertTrue(count==4);
	}
	
	@Test
	public void testPretrialMajorJDNationwide()throws Exception{
		Rete r = new Rete();
		r.reset();
		r.batch("DistributionEngine.clp");
		
		
		r.definstance("warrant", jdPreX, false);
		r.definstance("warrant", jdM, false);
		r.definstance("warrant", jd1, false);
		r.definstance("warrant", jd2, false);
		r.definstance("warrant", jd3, false);
		r.definstance("warrant", jd4, false);
		
		
		r.reset();
		r.run();		
		Iterator<ServiceBean> i = r.getObjects(new Filter.ByClass(ServiceBean.class));
		int count=0;
//		System.out.println("*******Majors-PR*******");
		while(i.hasNext()){
			count++;
			ServiceBean sb = i.next();
			assertTrue(sb.getServiceType().equals("nationwide"));
//			System.out.println("FileName: "+ sb.getFileName() + " to: "+sb.getRecipient());
		}
		assertTrue(count==6);
		
	}
	@Test
	public void testNotPretrialMajorJDStatewide()throws Exception{
		Rete r = new Rete();
		r.reset();
		r.batch("DistributionEngine.clp");
		
		
		r.definstance("warrant", jdPostX, false);
//		r.definstance("warrant", jdM, false);
//		r.definstance("warrant", jd1, false);
//		r.definstance("warrant", jd2, false);
//		r.definstance("warrant", jd3, false);
//		r.definstance("warrant", jd4, false);
		
		
		r.reset();
		r.run();		
		Iterator<ServiceBean> i = r.getObjects(new Filter.ByClass(ServiceBean.class));
		int count=0;
//		System.out.println("*******Majors-PR*******");
		while(i.hasNext()){
			count++;
			ServiceBean sb = i.next();
			assertTrue(sb.getServiceType().equals("statewide"));
//			System.out.println("FileName: "+ sb.getFileName() + " to: "+sb.getRecipient());
		}
		assertTrue(count==1);
		
	}
	@Test
	public void testMinorPostJDSurrounding()throws Exception{
		Rete r = new Rete();
		r.reset();
		r.batch("DistributionEngine.clp");
		
		
		r.definstance("warrant", jdAPost, false);
		
		r.reset();
		r.run();		
		Iterator<ServiceBean> i = r.getObjects(new Filter.ByClass(ServiceBean.class));
		int count=0;
//		System.out.println("*******Majors-PR*******");
		while(i.hasNext()){
			count++;
			ServiceBean sb = i.next();
			assertTrue(sb.getServiceType().equals("surrounding-counties")
					||sb.getServiceType().equalsIgnoreCase("surrounding-recipient"));
			if(sb.getServiceType().equalsIgnoreCase("surrounding-recipient")){
//				System.out.println("Surrounding Recipient: " + sb.getRecipient() + " file name: " + sb.getFileName());
			}
//			
//			System.out.println("FileName: "+ sb.getFileName() + " to: "+sb.getRecipient());
		}
		assertTrue(count>=1);
		
	}
	@Test
	public void testMinorPreJDGapAlert()throws Exception{
		Rete r = new Rete();
		r.reset();
		r.batch("DistributionEngine.clp");
		
		
		r.definstance("warrant", jdAPre, false);
		
		r.reset();
		r.run();		
		Iterator<ServiceBean> i = r.getObjects(new Filter.ByClass(ServiceBean.class));
		int count=0;
//		System.out.println("*******Majors-PR*******");
		while(i.hasNext()){
			fail("No service should be found");
		}
		Iterator<OperatorAlertBean> i2 = r.getObjects(new Filter.ByClass(OperatorAlertBean.class));
		count=0;
//		r.executeCommand("(facts)");
//		System.out.println("*******Majors-PR*******");
		while(i2.hasNext()){
			count++;
			OperatorAlertBean oab = i2.next();
			assertTrue(oab.getAlertCause().equals("no-service-found"));
		}
//		System.out.println("Count=="+count);
		assertTrue(count==1);
	}		
}
