package us.joshorr.jesswarrants;

public class WarrantBean {
	String caseType;
	String casePhase;
	String offenseClass;
	String county;
	String fileName;
	String caseNumber;
	
	public WarrantBean(){
		super();
	}
	
	
	
	public WarrantBean(String caseType, String casePhase, String offenseClass,
			String county, String fileName, String caseNumber) {
		super();
		this.caseType = caseType;
		this.casePhase = casePhase;
		this.offenseClass = offenseClass;
		this.county = county;
		this.fileName = fileName;
		this.caseNumber = caseNumber;
	}



	public String getCaseNumber() {
		return caseNumber;
	}
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	public String getCaseType() {
		return caseType;
	}
	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}
	public String getCasePhase() {
		return casePhase;
	}
	public void setCasePhase(String casePhase) {
		this.casePhase = casePhase;
	}
	public String getOffenseClass() {
		return offenseClass;
	}
	public void setOffenseClass(String offenseClass) {
		this.offenseClass = offenseClass;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	
}
