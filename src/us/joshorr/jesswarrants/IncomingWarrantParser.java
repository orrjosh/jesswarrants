package us.joshorr.jesswarrants;
import java.io.File;
import java.text.ParsePosition;
import java.util.Iterator;
import java.util.List;

import jess.Filter;
import jess.Rete;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.util.PDFTextStripper;

import us.joshorr.harmonydistribution.County;
import us.joshorr.harmonydistribution.Distributor;
import us.joshorr.harmonydistribution.State;


public class IncomingWarrantParser implements Runnable{
	
	PDDocument warrantDoc;
	String fileName;
	public IncomingWarrantParser(PDDocument doc, String fileName){
		this.warrantDoc=doc;
		this.fileName=fileName;
		
	}
	
	public WarrantBean parsePdfWarrant()throws Exception{
		WarrantBean wb = new WarrantBean();
		wb.setFileName(fileName);
		PDDocumentCatalog doccat = warrantDoc.getDocumentCatalog();
		PDAcroForm acroForm = doccat.getAcroForm();
		if (acroForm != null) {
			
			List<PDField> fields = acroForm.getFields();
			String defName,offense;
			for (PDField field : fields) {
//				System.out.println("field name= "
//						+ field.getFullyQualifiedName());
//				System.out.println("field data = " + field.getValue());
				if(field.getFullyQualifiedName().equalsIgnoreCase("caseNumber")){
					wb.setCaseNumber(field.getValue().trim());
				}else if(field.getFullyQualifiedName().equalsIgnoreCase("CaseType")){
					wb.setCaseType(field.getValue().trim());
				}else if(field.getFullyQualifiedName().equalsIgnoreCase("offenseClass")){
					wb.setOffenseClass(field.getValue().trim());
				}else if(field.getFullyQualifiedName().equalsIgnoreCase("casePhase")){
					wb.setCasePhase(field.getValue().trim());
				}else if(field.getFullyQualifiedName().equalsIgnoreCase("county")){
					wb.setCounty(field.getValue().trim());
				}else if(field.getFullyQualifiedName().equalsIgnoreCase("defendantName")){
					defName=field.getValue().trim();
				}else if(field.getFullyQualifiedName().equalsIgnoreCase("caseNumber")){
					offense = field.getValue().trim();
				}
			}
		}
		return wb;
	}
	public void executeReteEngine(WarrantBean wb)throws Exception{
		Rete r = new Rete();
		r.batch("DistributionEngine.clp");
		r.definstance("warrant", wb, false);
		r.reset();
		r.run();
		Iterator<OperatorAlertBean> alertIterator = r.getObjects(new Filter.ByClass(OperatorAlertBean.class));
		while(alertIterator.hasNext()){
			OperatorAlertBean oab = alertIterator.next();
			System.out.println("************OPERATOR ALERT****************");
			System.out.println("Alert Cause: "+oab.getAlertCause() + " File Name: "+oab.getFileName());
		}
		Iterator<ServiceBean> i = r.getObjects(new Filter.ByClass(ServiceBean.class));
		int count=0;
		while(i.hasNext()){
			count++;
			ServiceBean sb = i.next();
			System.out.println("Service Type: "+ sb.getServiceType()+" FileName: "+ sb.getFileName() + " to: "+sb.getRecipient());
			//MFT Connection
//			if(sb.getServiceType().equalsIgnoreCase("nationwide")){
//				for(State s: State.values()){
//					Distributor d = new Distributor(sb.getFileName(), s.toString());
//					d.send();
//					d=null;
//				}
//			}else if(sb.getServiceType().equalsIgnoreCase("statewide")){
//				for(County c: County.values()){
//					Distributor d = new Distributor(sb.getFileName(), c.toString());
//					d.send();
//					d=null;
//				}
//			}else if(sb.getServiceType().startsWith("surrounding")){
//				for(County c: County.values()){
//					if(sb.getRecipient().toUpperCase().equals(c.toString())){
//						Distributor d = new Distributor(sb.getFileName(), c.toString());
//						d.send();
//						d=null;
//						break;
//					}
//				}
//			}
		}
	}
	@Override
	public void run() {
		try{
			WarrantBean wb = parsePdfWarrant();
			executeReteEngine(wb);
			System.out.println("******************************************");
			warrantDoc.close();	
		}catch(Exception e){
			e.printStackTrace();
		}
		 
		
	}
//	public static void main(String args[]) throws Exception{
//		String fileName = "sampleWarrant.pdf";
//		PDDocument doc = PDDocument.load(new File(fileName));
//		IncomingWarrantParser warrantParser=new IncomingWarrantParser(doc,fileName);
//		WarrantBean wb = warrantParser.parsePdfWarrant();
//		warrantParser.executeReteEngine(wb);
//	}

}
